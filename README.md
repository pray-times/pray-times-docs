# Main Page

### From Pray Times

Welcome to **Pray Times**, an Islamic project aimed at providing an
open-source library for calculating Muslim prayers times.

The first version of Pray Times was released in early 2007. The code is
currently used in a wide range of Islamic websites and applications.

\
**Code and Manual:**

-   [How to calculate prayer
    times?](calculation.md)
-   [Pray Times code](https://gitlab.com/pray-times)
-   [Manual and examples](man.md)



**Features**:

-   Various methods of time calculation
-   Countdown to each prayer time
-   Automatic playing of Adhan sound
-   Supporting all locations around the world
-   Local calculation of prayer times (no connection to Internet is
    needed)

>Copyright © 2007-2021 PrayTimes.org \
>![CC-BY-NC 3.0](https://licensebuttons.net/l/by-nc/3.0/88x31.png)
