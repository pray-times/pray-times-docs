# A note on Dhuhr

### From Pray Times

Dhuhr has been defined in several ways in the fiqh literature:

1.  When the Sun begins to decline (Zawaal) after reaching its highest
    point in the sky.
2.  When the shadow of an indicator (a vertical stick) reaches its
    minimum length and starts to increase.
3.  When the Sun\\'s disk comes out of its zenith line, which is a line
    between the observer and the center of the Sun when it is at the
    highest point.

The first and the second definitions are equivalent, as the shadow
length has a direct correlation to the Sun\\'s elevation in the sky via
the following formula:

    Shadow Length = Object Height × cot(Sun Angle).

The Sun\\'s angle is a continuous function over time and has only one
peak point (the point at which the tangent to its curve has zero slope)
which is realized exactly at midday. Therefore, according to the first
two definitions, Dhuhr begins immediately after the midday.

The third definition is slightly different from the previous two
definitions. According to this definition, Sun must passes its zenith
line before Dhuhr starts. We need the following information to calculate
this time:

-   Sun radius (r): 695,500 km
-   Sun-Earth distance (d): 147,098,074 km to 152,097,701 km

Having r and d, the time t needed for Sun to pass its zenith line can be
computed using the following formula:

    t = arctan(r/d) / 2π × 24 × 60 × 60

The maximum value obtained by the above formula (which corresponds to
the minimum Sun-Earth distance) is 65.0 seconds. Therefore, it takes
approximately 1 minute until Sun\\'s disk comes out of its zenith that
should be considered into consideration for calculating Dhuhr, if the
third definition is used.

## Conclusion

There are three definitions for the Dhuhr time as described above.
According to the first two definitions, Dhuhr = midday, and according to
the third definition, Dhuhr = midday + 65 sec.

## References

-   [Sun](http://en.wikipedia.org/wiki/Sun), from
    Wikipedia, the free encyclopedia.
-   [Sun-Earth
    Distance](http://wiki.answers.com/Q/What_is_the_distance_from_the_Sun_to_the_Earth), from Wiki Answers.

> Retrieved from <http://praytimes.org/wiki/A_note_on_Dhuhr>
