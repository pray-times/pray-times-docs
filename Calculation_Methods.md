# Calculation Methods

There are different conventions for calculating prayer times. The
following table lists several well-known conventions currently in use in
various regions:

| Method                                            | Abbr.   | Region Used                                 |
| :------------------------------------------------ | :------ | :------------------------------------------ |
| Muslim World League                               | MWL     | Europe, Far East, parts of US               |
| Islamic Society of North America                  | ISNA    | North America (US and Canada)               |
| Egyptian General Authority of Survey              | Egypt   | Africa, Syria, Lebanon, Malaysia            |
| Umm al-Qura University, Makkah                    | Makkah  | Arabian Peninsula                           |
| University of Islamic Sciences, Karachi           | Karachi |     Pakistan, Afganistan, Bangladesh, India |
| Institute of Geophysics, University of Tehran     | Tehran  | Iran, Some Shia communities                 |
| Shia Ithna Ashari, Leva Research Institute, Qum   | Jafari  | Some Shia communities worldwide             |

### Calculating Parameters

The table below shows the default calculating parameters for each
calculation method:

| Method   | Fajr Angle | Isha                                           | Maghrib  | Midnight                                                                        |
| :------- | :--------- | :--------------------------------------------- | :------- | :------------------------------------------------------------------------------ |
| MWL      | 18°        | 17°                                            | = Sunset | mid Sunset to                                                        Sunrise    |
| ISNA     | 15°        | 15°                                            | = Sunset | mid Sunset to Sunrise                                                           |
| Egypt    | 19.5°      | 17.5°                                          | = Sunset | mid Sunset to           Sunrise                                                 |
| Makkah   | 18.5°      | 90 min after Maghrib,  120 min during  Ramadan | = Sunset | mid Sunset to               Sunrise                                             |
| Karachi  | 18°        | 18°                                            | = Sunset | mid Sunset to                                                           Sunrise |
| Tehran   | 17.7°      | 14°                                            | 4.5°     | mid Sunset to                                                         Fajr      |
| Jafari   | 16°        | 14°                                            | 4°       | mid Sunset to                                                              Fajr |

* * *

> **Notes**:
>
> -   Fajr angle was 19 degrees in Umm al-Qura method before Muharram
>     1430 Hijri (December 2008).
> -   Isha angle is not explicitly specified in Tehran method.
>
> Retrieved from <http://praytimes.org/wiki/Calculation_Methods>
